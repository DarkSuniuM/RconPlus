import time
import socket
import re
import sqlite3
from webbrowser import open as webopen
from valve import rcon as vrc
from sys import argv,exit
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication,QPlainTextEdit,QVBoxLayout,\
    QHBoxLayout,QComboBox, QLabel,QLineEdit,QPushButton,QMessageBox,QCheckBox, QAction,qApp
from PyQt5.QtGui import QIcon
from urllib.request import urlopen


## VERSION ##
version = '0.1beta'
last_version = urlopen('https://gamerplus.ir/rconplus/last_version.txt').read().decode()


## DATABASE STUFF ##

db = sqlite3.connect('SavedServers.db')
dbc = db.cursor()
dbc.execute("CREATE TABLE IF NOT EXISTS Servers ("
                 "Server_ID INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL ,"
                 "Type TEXT,"
                 "Server_IP TEXT,"
                 "Server_Port INTEGER,"
                 "HostName TEXT,"
                 "RCon_Password TEXT,"
                 "IP_Port Text UNIQUE,"
                 "Name TEXT)")
db.commit()
dbc.execute("SELECT * FROM Servers WHERE Type='Quake 3'")
q3sv = dbc.fetchall()
dbc.execute("SELECT * FROM Servers WHERE Type='Valve'")
vasv = dbc.fetchall()


class RemoteCon(QWidget):
    def __init__(self):
        super().__init__()
        # Message Box #
        self.mbox = QMessageBox()
        self.mbox.resize(300,100)
        self.mbox.setWindowIcon(QIcon('RCon Plus125.png'))
        self.mbox.setWindowTitle('Message!')

        # Connection Settings #
        self.netTypeItems = ("Quake 3","Valve")
        self.lab_NetType = QLabel("Network Layer Type:")
        self.get_NetType = QComboBox()
        self.get_NetType.setMaximumWidth(180)
        self.get_NetType.addItems(self.netTypeItems)
        self.lab_IP = QLabel("Server IP:")
        self.get_IP = QLineEdit()
        self.get_IP.setMaximumWidth(180)
        self.lab_Port = QLabel("Query Port:")
        self.get_Port = QLineEdit()
        self.get_Port.setMaximumWidth(180)
        self.lab_PW = QLabel("RCon Password:")
        self.get_PW = QLineEdit()
        self.get_PW.setEchoMode(QLineEdit.Password)
        self.get_PW.setMaximumWidth(180)
        self.saveCon = QCheckBox('Save this Server')
        self.connect = QPushButton("Connect")
        self.connect.setMaximumWidth(180)
        self.disconn = QPushButton("Disconnect")
        self.disconn.setMaximumWidth(180)
        self.disconn.setHidden(True)

        # Console
        self.lab_COut = QLabel("Console Responce:")
        self.get_COut = QPlainTextEdit()
        self.get_COut.setMinimumWidth(500)
        self.get_COut.setReadOnly(True)
        self.lab_CIn = QLabel("Command:")
        self.set_CIn = QLineEdit()
        self.send_CIn = QPushButton("Send Command")
        self.clearC = QPushButton("Clear Console")

        # Connection Status #
        self.Status = QLabel("Status: Not Connected")
        self.SvHostName = QLabel('HostName: -')
        self.MaxPlayers = QLabel("Maximum Players: -")
        self.Map = QLabel("Map: -")
        self.reStatus = QPushButton("Refresh Status")

        # Label Developer #
        self.developer = QLabel("Developed by <a href='https://gamerplus.ir'>GamerPlus.ir</a>")
        self.developer.setOpenExternalLinks(True)

        # Begin Layouts

        # Send Command and Clear #

        vLaySub = QHBoxLayout()
        vLaySub.addWidget(self.send_CIn)
        vLaySub.addWidget(self.clearC)

        # Vertical Layout #1 #
        vLay1 = QVBoxLayout()
        vLay1.addWidget(self.lab_NetType)
        vLay1.addWidget(self.get_NetType)
        vLay1.addWidget(self.lab_IP)
        vLay1.addWidget(self.get_IP)
        vLay1.addWidget(self.lab_Port)
        vLay1.addWidget(self.get_Port)
        vLay1.addWidget(self.lab_PW)
        vLay1.addWidget(self.get_PW)
        vLay1.addWidget(self.saveCon)
        vLay1.addWidget(self.connect)
        vLay1.addWidget(self.disconn)

        # Vertical Layout #2 #
        vLay2 = QVBoxLayout()
        vLay2.addWidget(self.lab_COut)
        vLay2.addWidget(self.get_COut)
        vLay2.addWidget(self.lab_CIn)
        vLay2.addWidget(self.set_CIn)
        vLay2.addLayout(vLaySub)

        # Vertical Layout #3 #
        vLay3 = QVBoxLayout()
        vLay3.addWidget(self.Status)
        vLay3.addWidget(self.SvHostName)
        vLay3.addWidget(self.MaxPlayers)
        vLay3.addWidget(self.Map)
        vLay3.addWidget(self.reStatus)

        # Horizonal Layout #
        hLay = QHBoxLayout()
        hLay.addLayout(vLay1)
        hLay.addLayout(vLay3)

        # Vertical  All #
        vLay4 = QVBoxLayout()
        vLay4.addLayout(hLay)
        vLay4.addLayout(vLay2)
        vLay4.addWidget(self.developer)

        # End Layouts

        # Variables #
        self.ConSv = self.get_IP.text
        self.ConPort = self.get_Port.text
        self.ConPw = self.get_PW.text

        # Signals #
        self.connect.clicked.connect(lambda: self.Con(self.ConSv(),self.ConPort(),self.get_NetType.currentText(),self.ConPw()))
        self.disconn.clicked.connect(self.Dis)
        self.send_CIn.clicked.connect(lambda: self.Console(self.set_CIn.text(),self.ConSv(),self.ConPort(),self.get_NetType.currentText(),self.ConPw()))
        self.clearC.clicked.connect(self.get_COut.clear)
        self.reStatus.clicked.connect(self.ReStats)
        self.set_CIn.returnPressed.connect(lambda: self.Console(self.set_CIn.text(),self.ConSv(),self.ConPort(),self.get_NetType.currentText(),self.ConPw()))
        self.get_PW.returnPressed.connect(lambda: self.Con(self.ConSv(),self.ConPort(),self.get_NetType.currentText(),self.ConPw()))

        # Application Settings #
        self.setLayout(vLay4)
        self.setWindowTitle("RCON Plus")
        self.show()

    def chkupdate(self):
        if version == last_version:
            self.mbox.setText('You are already using the latest version!!')
            self.mbox.show()
        else:
            self.mbox.setText('A Newer version is availabe for you!\n<a href="https://gamerplus.ir/rconplus/">Update Now!</a>')
            self.show()

    def consavesv(self,texto):
        texto = texto.split(' | ')
        texto = texto[0]
        dbc.execute("SELECT Type, Server_IP, Server_Port, RCon_Password FROM Servers WHERE IP_Port=?", (texto,))
        texto = dbc.fetchall()
        self.get_IP.setText(texto[0][1])
        self.get_Port.setText(str(texto[0][2]))
        self.get_NetType.setCurrentText(texto[0][0])
        self.get_PW.setText(texto[0][3])

    def savesv(self):
        TYPE = self.get_NetType.currentText()
        IP = self.get_IP.text()
        PORT = self.get_Port.text()
        if TYPE == self.netTypeItems[0]: HOSTNAME = self.matches[8]
        elif TYPE == self.netTypeItems[1]: HOSTNAME = self.matches[0]
        RCON_PW = self.get_PW.text()
        dbc.execute('INSERT INTO Servers (Type,Server_IP,Server_Port,HostName,RCon_Password,IP_Port,Name) VALUES (?, ?, ?, ?, ?, ?, ?)',
                         (TYPE,IP,PORT,HOSTNAME,RCON_PW,IP+":"+str(PORT),IP+":"+str(PORT)+" | "+HOSTNAME))
        db.commit()

    def updatesv(self):
        TYPE = self.get_NetType.currentText()
        IP = self.get_IP.text()
        PORT = self.get_Port.text()
        if TYPE == self.netTypeItems[0]:
            HOSTNAME = self.matches[8]
        elif TYPE == self.netTypeItems[1]:
            HOSTNAME = self.matches[0]
        RCON_PW = self.get_PW.text()
        dbc.execute(
            'UPDATE Servers SET Type=?,Server_IP=?,Server_Port=?,HostName=?,RCon_Password=?,IP_Port=?,Name=? WHERE IP_Port=?',
            (TYPE, IP, PORT, HOSTNAME, RCON_PW, IP + ":" + str(PORT), IP + ":" + str(PORT) + " | " + HOSTNAME,IP + ":" + str(PORT)))
        db.commit()

    def stats(self, status='Not Connected',hostname='-',maxplayers='-',map='-'):
        self.Status.setText("Status: {}".format(status))
        self.SvHostName.setText("HostName: {}".format(hostname))
        self.MaxPlayers.setText("Maximum Players: {}".format(maxplayers))
        self.Map.setText("Map: {}".format(map))

    def recv_timeout(self,the_socket, timeout=2):
        the_socket.setblocking(0)
        total_data = [];
        data = '';
        begin = time.time()
        while 1:
            # if you got some data, then break after wait sec
            if total_data and time.time() - begin > timeout:
                break
            # if you got no data at all, wait a little longer
            elif time.time() - begin > timeout * 2:
                break
            try:
                data = the_socket.recv(8192)[10:]
                if data:
                    total_data.append(data)
                    begin = time.time()
                else:
                    time.sleep(0.1)
            except:
                pass
        return b''.join(total_data)

    def Con(self, server='127.0.0.1', port='27015', nettype='Automatic', passwd=''):
        try:
            self.qprefix = b'\xff\xff\xff\xffrcon "'
            self.addr = (server,int(port))
            self.qpw = bytes(passwd,'UTF-8')
            self.vpw = str(passwd)
            if nettype == self.netTypeItems[0]:
                self.qregex = r"^.*?\s([^\s].*)$"
                try:
                    self.rcon = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    self.rcon.connect(self.addr)
                    self.rcon.send(self.qprefix + self.qpw + b'" ' + b'serverinfo')
                    qsts = self.recv_timeout(self.rcon).decode()
                    if "The server must set 'rcon_password' for clients to use 'rcon'." in qsts:
                        self.mbox.setText('Error => This server is not using RCon')
                        self.mbox.show()
                    elif qsts == '':
                        self.mbox.setText('Error => Connection Failed!')
                        self.mbox.show()
                    elif "Invalid password." in qsts:
                        self.mbox.setText('Error => RCon password is wrong!')
                        self.mbox.show()
                    else:
                        self.matches = re.findall(self.qregex,qsts,re.MULTILINE)
                        self.stats("<font color='green'>Connected</font>",self.matches[8],self.matches[9],self.matches[3])
                        if self.saveCon.isChecked() == True:
                            try:
                                self.savesv()
                            except sqlite3.IntegrityError:
                                self.updatesv()
                                pass
                        self.get_NetType.setDisabled(True)
                        self.get_IP.setDisabled(True)
                        self.get_Port.setDisabled(True)
                        self.get_PW.setDisabled(True)
                        self.saveCon.setDisabled(True)
                        self.connect.setHidden(True)
                        self.disconn.setHidden(False)
                except Exception as e:
                    self.mbox.setText('Error => {}'.format(e))
                    self.mbox.show()
            elif nettype == self.netTypeItems[1]:
                self.vregex = r"^.*?:\s+(.*)$"
                try:
                    self.vsts = vrc.execute(self.addr,self.vpw,'status')
                    self.matches = re.findall(self.vregex,self.vsts,re.MULTILINE)
                    self.stats("<font color='green'>Connected</font>", self.matches[0], self.matches[6], self.matches[5])
                    if self.saveCon.isChecked() == True:
                        try:
                            self.savesv()
                        except sqlite3.IntegrityError:
                            self.updatesv()
                    self.get_NetType.setDisabled(True)
                    self.get_IP.setDisabled(True)
                    self.get_Port.setDisabled(True)
                    self.get_PW.setDisabled(True)
                    self.saveCon.setDisabled(True)
                    self.connect.setHidden(True)
                    self.disconn.setHidden(False)
                except Exception as e:
                    self.mbox.setText('Error# => {}'.format(e))
                    self.mbox.show()
        except ValueError:
            self.mbox.setText('Error => Invalid Value!\nCheck IP or Port')
            self.mbox.show()

    def ReStats(self):
        if self.Status.text() == "Status: Not Connected":
            self.mbox.setText('You need to connect to a server!')
            self.mbox.show()
        else:
            if self.get_NetType.currentText() == self.netTypeItems[0]:
                self.rcon.send(self.qprefix + self.qpw + b'" ' + b'serverinfo')
                qsts = self.recv_timeout(self.rcon).decode()
                self.matches = re.findall(self.qregex, qsts, re.MULTILINE)
                self.stats("<font color='green'>Connected</font>", self.matches[8], self.matches[9], self.matches[3])
            elif self.get_NetType.currentText() == self.netTypeItems[1]:
                try:
                    self.vsts = vrc.execute(self.addr,self.vpw,'status')
                    self.matches = re.findall(self.vregex,self.vsts,re.MULTILINE)
                    self.stats("<font color='green'>Connected</font>", self.matches[0], self.matches[6], self.matches[5])
                except Exception:
                    self.mbox.setText('You need to connect to a server!')
                    self.mbox.show()

    def get_src(self):
        src_url = 'https://gamerplus.ir/rconplus/src/{}.zip'.format(version)
        webopen(src_url,new=0,autoraise=True)

    def showabout(self):
        self.mbox.setText('<div style="text-align:center;"><img src="RCON Plus125.png"><br>RCon Plus | v: {}<br>Developed by <a href="https://gamerplus.ir">GamerPlus</a> using <a href="https://python.org">Python3</a> with ♥ for all GameServer Managers<br><br>Contact:<br><a href="mailto:rconplus@gamerplus.ir">RConPlus@GamerPlus.ir</a><br><a href="https://t.me/gamerplus_ir">GamerPlus on Telegram</a></div>'.format(version))
        self.mbox.setContentsMargins(-20,0,0,0)
        self.mbox.show()

    def Console(self,cmd,server,port,nettype,passwd=''):
        try:
            if nettype==self.netTypeItems[0]:
                cmd = bytes(cmd,'UTF-8')
                self.rcon.send(self.qprefix+self.qpw+b'" '+cmd)
                self.get_COut.appendPlainText('$ {}:\n{}'.format(self.set_CIn.text(),self.recv_timeout(self.rcon).decode()))
                self.set_CIn.clear()
            elif nettype==self.netTypeItems[1]:
                self.get_COut.appendPlainText('$ {}:\n{}'.format(self.set_CIn.text(),vrc.execute(self.addr,self.vpw,cmd)))
                self.set_CIn.clear()
            else:
                pass
        except Exception as e:
            self.mbox.setText('You need to connect to a server because:\n {}'.format(e))
            self.mbox.show()

    def Dis(self):
        try:
            self.rcon.close()
        except Exception as e:
            pass
        self.stats()
        self.get_COut.clear()
        self.get_NetType.setDisabled(False)
        self.get_IP.setDisabled(False)
        self.get_Port.setDisabled(False)
        self.get_PW.setDisabled(False)
        self.saveCon.setDisabled(False)
        self.get_IP.clear()
        self.get_Port.clear()
        self.get_PW.clear()
        self.saveCon.setChecked(False)
        self.connect.setHidden(False)
        self.disconn.setHidden(True)

class MainWin(QMainWindow):
    def __init__(self):
        super(MainWin,self).__init__()
        self.main_form = RemoteCon()
        self.setCentralWidget(self.main_form)
        self.unit_ui()

    def unit_ui(self):
        # Menu Bars #
        bar = self.menuBar()

        # Application Menu #
        app_bar = bar.addMenu('Application')
        exit_menu = QAction('&Exit', self)
        exit_menu.setShortcut('Ctrl+Q')

        connect_menu = app_bar.addMenu('Connect To')
        app_bar.addAction(exit_menu)

        # Connect To #
            # Quake 3
        q3 = connect_menu.addMenu('Quake 3')
        q3menus = {}
        x = 0
        for element in q3sv:
            key = 'Q'+str(x)
            q3menus[key] = QAction(element[7], self)
            q3.addAction(q3menus[key])
            q3menus[key].triggered.connect(lambda checked, key=key : self.main_form.consavesv(q3menus[key].text()))
            x += 1

            # Vavle
        va = connect_menu.addMenu('Valve')
        vamenus = {}
        x = 0
        for element in vasv:
            key = 'V'+str(x)
            vamenus[key] = QAction(element[7], self)
            va.addAction(vamenus[key])
            vamenus[key].triggered.connect(lambda checked, key=key: self.main_form.consavesv(vamenus[key].text()))


        # Help Menu #
        help_bar = bar.addMenu('Help')
        source_menu = QAction('Get Source!', self)
        update_menu = QAction('Check for Updates', self)
        update_menu.setShortcut('Ctrl+U')
        about_menu = QAction('&About', self)

        help_bar.addAction(source_menu)
        help_bar.addAction(update_menu)
        help_bar.addAction(about_menu)

        # Menu Signals #
        exit_menu.triggered.connect(qApp.exit)
        update_menu.triggered.connect(self.main_form.chkupdate)
        about_menu.triggered.connect(self.main_form.showabout)
        source_menu.triggered.connect(self.main_form.get_src)

        self.setWindowTitle('RCon Plus')
        self.setWindowIcon(QIcon('RCon Plus125.png'))
        self.show()

app = QApplication(argv)
window = MainWin()
exit(app.exec_())
=======
import time
import socket
import re
import sqlite3
from webbrowser import open as webopen
from valve import rcon as vrc
from sys import argv,exit
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication,QPlainTextEdit,QVBoxLayout,\
    QHBoxLayout,QComboBox, QLabel,QLineEdit,QPushButton,QMessageBox,QCheckBox, QAction,qApp
from PyQt5.QtGui import QIcon
from urllib.request import urlopen


## VERSION ##
version = '0.1beta'
last_version = urlopen('https://raw.githubusercontent.com/DarkSuniuM/RconPlus/master/last_version.txt').read().decode()


## DATABASE STUFF ##

db = sqlite3.connect('SavedServers.db')
dbc = db.cursor()
dbc.execute("CREATE TABLE IF NOT EXISTS Servers ("
                 "Server_ID INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL ,"
                 "Type TEXT,"
                 "Server_IP TEXT,"
                 "Server_Port INTEGER,"
                 "HostName TEXT,"
                 "RCon_Password TEXT,"
                 "IP_Port Text UNIQUE,"
                 "Name TEXT)")
db.commit()
dbc.execute("SELECT * FROM Servers WHERE Type='Quake 3'")
q3sv = dbc.fetchall()
dbc.execute("SELECT * FROM Servers WHERE Type='Valve'")
vasv = dbc.fetchall()


# GUI Stuff :| #
class RemoteCon(QWidget):
    def __init__(self):
        super().__init__()
        # Message Box #
        self.mbox = QMessageBox()
        self.mbox.resize(300,100)
        self.mbox.setWindowIcon(QIcon('RCon Plus125.png'))
        self.mbox.setWindowTitle('Message!')

        # Connection Settings #
        self.netTypeItems = ("Quake 3","Valve")
        self.lab_NetType = QLabel("Network Layer Type:")
        self.get_NetType = QComboBox()
        self.get_NetType.setMaximumWidth(180)
        self.get_NetType.addItems(self.netTypeItems)
        self.lab_IP = QLabel("Server IP:")
        self.get_IP = QLineEdit()
        self.get_IP.setMaximumWidth(180)
        self.lab_Port = QLabel("Query Port:")
        self.get_Port = QLineEdit()
        self.get_Port.setMaximumWidth(180)
        self.lab_PW = QLabel("RCon Password:")
        self.get_PW = QLineEdit()
        self.get_PW.setEchoMode(QLineEdit.Password)
        self.get_PW.setMaximumWidth(180)
        self.saveCon = QCheckBox('Save this Server')
        self.connect = QPushButton("Connect")
        self.connect.setMaximumWidth(180)
        self.disconn = QPushButton("Disconnect")
        self.disconn.setMaximumWidth(180)
        self.disconn.setHidden(True)

        # Console
        self.lab_COut = QLabel("Console Responce:")
        self.get_COut = QPlainTextEdit()
        self.get_COut.setMinimumWidth(500)
        self.get_COut.setReadOnly(True)
        self.lab_CIn = QLabel("Command:")
        self.set_CIn = QLineEdit()
        self.send_CIn = QPushButton("Send Command")
        self.clearC = QPushButton("Clear Console")

        # Connection Status #
        self.Status = QLabel("Status: Not Connected")
        self.SvHostName = QLabel('HostName: -')
        self.MaxPlayers = QLabel("Maximum Players: -")
        self.Map = QLabel("Map: -")
        self.reStatus = QPushButton("Refresh Status")

        # Label Developer #
        self.developer = QLabel("Developed by <a href='https://gamerplus.ir'>GamerPlus.ir</a>")
        self.developer.setOpenExternalLinks(True)

        # Begin Layouts

        # Send Command and Clear #

        vLaySub = QHBoxLayout()
        vLaySub.addWidget(self.send_CIn)
        vLaySub.addWidget(self.clearC)

        # Vertical Layout #1 #
        vLay1 = QVBoxLayout()
        vLay1.addWidget(self.lab_NetType)
        vLay1.addWidget(self.get_NetType)
        vLay1.addWidget(self.lab_IP)
        vLay1.addWidget(self.get_IP)
        vLay1.addWidget(self.lab_Port)
        vLay1.addWidget(self.get_Port)
        vLay1.addWidget(self.lab_PW)
        vLay1.addWidget(self.get_PW)
        vLay1.addWidget(self.saveCon)
        vLay1.addWidget(self.connect)
        vLay1.addWidget(self.disconn)

        # Vertical Layout #2 #
        vLay2 = QVBoxLayout()
        vLay2.addWidget(self.lab_COut)
        vLay2.addWidget(self.get_COut)
        vLay2.addWidget(self.lab_CIn)
        vLay2.addWidget(self.set_CIn)
        vLay2.addLayout(vLaySub)

        # Vertical Layout #3 #
        vLay3 = QVBoxLayout()
        vLay3.addWidget(self.Status)
        vLay3.addWidget(self.SvHostName)
        vLay3.addWidget(self.MaxPlayers)
        vLay3.addWidget(self.Map)
        vLay3.addWidget(self.reStatus)

        # Horizonal Layout #
        hLay = QHBoxLayout()
        hLay.addLayout(vLay1)
        hLay.addLayout(vLay3)

        # Vertical  All #
        vLay4 = QVBoxLayout()
        vLay4.addLayout(hLay)
        vLay4.addLayout(vLay2)
        vLay4.addWidget(self.developer)

        # End Layouts

        # Variables #
        self.ConSv = self.get_IP.text
        self.ConPort = self.get_Port.text
        self.ConPw = self.get_PW.text

        # Signals #
        self.connect.clicked.connect(lambda: self.Con(self.ConSv(),self.ConPort(),self.get_NetType.currentText(),self.ConPw()))
        self.disconn.clicked.connect(self.Dis)
        self.send_CIn.clicked.connect(lambda: self.Console(self.set_CIn.text(),self.ConSv(),self.ConPort(),self.get_NetType.currentText(),self.ConPw()))
        self.clearC.clicked.connect(self.get_COut.clear)
        self.reStatus.clicked.connect(self.ReStats)
        self.set_CIn.returnPressed.connect(lambda: self.Console(self.set_CIn.text(),self.ConSv(),self.ConPort(),self.get_NetType.currentText(),self.ConPw()))
        self.get_PW.returnPressed.connect(lambda: self.Con(self.ConSv(),self.ConPort(),self.get_NetType.currentText(),self.ConPw()))

        # Application Settings #
        self.setLayout(vLay4)
        self.setWindowTitle("RCON Plus")
        self.show()

    def chkupdate(self):
        if version == last_version:
            self.mbox.setText('You are already using the latest version!!')
            self.mbox.show()
        else:
            self.mbox.setText('A Newer version is availabe for you!\n<a href="https://gamerplus.ir/rconplus/">Update Now!</a>')
            self.show()

    def consavesv(self,texto):
        texto = texto.split(' | ')
        texto = texto[0]
        dbc.execute("SELECT Type, Server_IP, Server_Port, RCon_Password FROM Servers WHERE IP_Port=?", (texto,))
        texto = dbc.fetchall()
        self.get_IP.setText(texto[0][1])
        self.get_Port.setText(str(texto[0][2]))
        self.get_NetType.setCurrentText(texto[0][0])
        self.get_PW.setText(texto[0][3])

    def savesv(self):
        TYPE = self.get_NetType.currentText()
        IP = self.get_IP.text()
        PORT = self.get_Port.text()
        if TYPE == self.netTypeItems[0]: HOSTNAME = self.matches[8]
        elif TYPE == self.netTypeItems[1]: HOSTNAME = self.matches[0]
        RCON_PW = self.get_PW.text()
        dbc.execute('INSERT INTO Servers (Type,Server_IP,Server_Port,HostName,RCon_Password,IP_Port,Name) VALUES (?, ?, ?, ?, ?, ?, ?)',
                         (TYPE,IP,PORT,HOSTNAME,RCON_PW,IP+":"+str(PORT),IP+":"+str(PORT)+" | "+HOSTNAME))
        db.commit()

    def updatesv(self):
        TYPE = self.get_NetType.currentText()
        IP = self.get_IP.text()
        PORT = self.get_Port.text()
        if TYPE == self.netTypeItems[0]:
            HOSTNAME = self.matches[8]
        elif TYPE == self.netTypeItems[1]:
            HOSTNAME = self.matches[0]
        RCON_PW = self.get_PW.text()
        dbc.execute(
            'UPDATE Servers SET Type=?,Server_IP=?,Server_Port=?,HostName=?,RCon_Password=?,IP_Port=?,Name=? WHERE IP_Port=?',
            (TYPE, IP, PORT, HOSTNAME, RCON_PW, IP + ":" + str(PORT), IP + ":" + str(PORT) + " | " + HOSTNAME,IP + ":" + str(PORT)))
        db.commit()

    def stats(self, status='Not Connected',hostname='-',maxplayers='-',map='-'):
        self.Status.setText("Status: {}".format(status))
        self.SvHostName.setText("HostName: {}".format(hostname))
        self.MaxPlayers.setText("Maximum Players: {}".format(maxplayers))
        self.Map.setText("Map: {}".format(map))

    def recv_timeout(self,the_socket, timeout=2):
        the_socket.setblocking(0)
        total_data = [];
        data = '';
        begin = time.time()
        while 1:
            # if you got some data, then break after wait sec
            if total_data and time.time() - begin > timeout:
                break
            # if you got no data at all, wait a little longer
            elif time.time() - begin > timeout * 2:
                break
            try:
                data = the_socket.recv(8192)[10:]
                if data:
                    total_data.append(data)
                    begin = time.time()
                else:
                    time.sleep(0.1)
            except:
                pass
        return b''.join(total_data)

    def Con(self, server='127.0.0.1', port='27015', nettype='Automatic', passwd=''):
        try:
            self.qprefix = b'\xff\xff\xff\xffrcon "'
            self.addr = (server,int(port))
            self.qpw = bytes(passwd,'UTF-8')
            self.vpw = str(passwd)
            if nettype == self.netTypeItems[0]:
                self.qregex = r"^.*?\s([^\s].*)$"
                try:
                    self.rcon = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    self.rcon.connect(self.addr)
                    self.rcon.send(self.qprefix + self.qpw + b'" ' + b'serverinfo')
                    qsts = self.recv_timeout(self.rcon).decode()
                    if "The server must set 'rcon_password' for clients to use 'rcon'." in qsts:
                        self.mbox.setText('Error => This server is not using RCon')
                        self.mbox.show()
                    elif qsts == '':
                        self.mbox.setText('Error => Connection Failed!')
                        self.mbox.show()
                    elif "Invalid password." in qsts:
                        self.mbox.setText('Error => RCon password is wrong!')
                        self.mbox.show()
                    else:
                        self.matches = re.findall(self.qregex,qsts,re.MULTILINE)
                        self.stats("<font color='green'>Connected</font>",self.matches[8],self.matches[9],self.matches[3])
                        if self.saveCon.isChecked() == True:
                            try:
                                self.savesv()
                            except sqlite3.IntegrityError:
                                self.updatesv()
                                pass
                        self.get_NetType.setDisabled(True)
                        self.get_IP.setDisabled(True)
                        self.get_Port.setDisabled(True)
                        self.get_PW.setDisabled(True)
                        self.saveCon.setDisabled(True)
                        self.connect.setHidden(True)
                        self.disconn.setHidden(False)
                except Exception as e:
                    self.mbox.setText('Error => {}'.format(e))
                    self.mbox.show()
            elif nettype == self.netTypeItems[1]:
                self.vregex = r"^.*?:\s+(.*)$"
                try:
                    self.vsts = vrc.execute(self.addr,self.vpw,'status')
                    self.matches = re.findall(self.vregex,self.vsts,re.MULTILINE)
                    self.stats("<font color='green'>Connected</font>", self.matches[0], self.matches[6], self.matches[5])
                    if self.saveCon.isChecked() == True:
                        try:
                            self.savesv()
                        except sqlite3.IntegrityError:
                            self.updatesv()
                    self.get_NetType.setDisabled(True)
                    self.get_IP.setDisabled(True)
                    self.get_Port.setDisabled(True)
                    self.get_PW.setDisabled(True)
                    self.saveCon.setDisabled(True)
                    self.connect.setHidden(True)
                    self.disconn.setHidden(False)
                except Exception as e:
                    self.mbox.setText('Error# => {}'.format(e))
                    self.mbox.show()
        except ValueError:
            self.mbox.setText('Error => Invalid Value!\nCheck IP or Port')
            self.mbox.show()

    def ReStats(self):
        if self.Status.text() == "Status: Not Connected":
            self.mbox.setText('You need to connect to a server!')
            self.mbox.show()
        else:
            if self.get_NetType.currentText() == self.netTypeItems[0]:
                self.rcon.send(self.qprefix + self.qpw + b'" ' + b'serverinfo')
                qsts = self.recv_timeout(self.rcon).decode()
                self.matches = re.findall(self.qregex, qsts, re.MULTILINE)
                self.stats("<font color='green'>Connected</font>", self.matches[8], self.matches[9], self.matches[3])
            elif self.get_NetType.currentText() == self.netTypeItems[1]:
                try:
                    self.vsts = vrc.execute(self.addr,self.vpw,'status')
                    self.matches = re.findall(self.vregex,self.vsts,re.MULTILINE)
                    self.stats("<font color='green'>Connected</font>", self.matches[0], self.matches[6], self.matches[5])
                except Exception:
                    self.mbox.setText('You need to connect to a server!')
                    self.mbox.show()

    def get_src(self):
        src_url = 'https://gamerplus.ir/rconplus/src/{}.zip'.format(version)
        webopen(src_url,new=0,autoraise=True)

    def showabout(self):
        self.mbox.setText('<div style="text-align:center;"><img src="RCON Plus125.png"><br>RCon Plus | v: {}<br>Developed by <a href="https://gamerplus.ir">GamerPlus</a> using <a href="https://python.org">Python3</a> with ♥ for all GameServer Managers<br><br>Contact:<br><a href="mailto:rconplus@gamerplus.ir">RConPlus@GamerPlus.ir</a><br><a href="https://t.me/gamerplus_ir">GamerPlus on Telegram</a></div>'.format(version))
        self.mbox.setContentsMargins(-20,0,0,0)
        self.mbox.show()

    def Console(self,cmd,server,port,nettype,passwd=''):
        try:
            if nettype==self.netTypeItems[0]:
                cmd = bytes(cmd,'UTF-8')
                self.rcon.send(self.qprefix+self.qpw+b'" '+cmd)
                self.get_COut.appendPlainText('$ {}:\n{}'.format(self.set_CIn.text(),self.recv_timeout(self.rcon).decode()))
                self.set_CIn.clear()
            elif nettype==self.netTypeItems[1]:
                self.get_COut.appendPlainText('$ {}:\n{}'.format(self.set_CIn.text(),vrc.execute(self.addr,self.vpw,cmd)))
                self.set_CIn.clear()
            else:
                pass
        except Exception as e:
            self.mbox.setText('You need to connect to a server because:\n {}'.format(e))
            self.mbox.show()

    def Dis(self):
        try:
            self.rcon.close()
        except Exception as e:
            pass
        self.stats()
        self.get_COut.clear()
        self.get_NetType.setDisabled(False)
        self.get_IP.setDisabled(False)
        self.get_Port.setDisabled(False)
        self.get_PW.setDisabled(False)
        self.saveCon.setDisabled(False)
        self.get_IP.clear()
        self.get_Port.clear()
        self.get_PW.clear()
        self.saveCon.setChecked(False)
        self.connect.setHidden(False)
        self.disconn.setHidden(True)

class MainWin(QMainWindow):
    def __init__(self):
        super(MainWin,self).__init__()
        self.main_form = RemoteCon()
        self.setCentralWidget(self.main_form)
        self.unit_ui()

    def unit_ui(self):
        # Menu Bars #
        bar = self.menuBar()

        # Application Menu #
        app_bar = bar.addMenu('Application')
        exit_menu = QAction('&Exit', self)
        exit_menu.setShortcut('Ctrl+Q')

        connect_menu = app_bar.addMenu('Connect To')
        app_bar.addAction(exit_menu)

        # Connect To #
            # Quake 3
        q3 = connect_menu.addMenu('Quake 3')
        q3menus = {}
        x = 0
        for element in q3sv:
            key = 'Q'+str(x)
            q3menus[key] = QAction(element[7], self)
            q3.addAction(q3menus[key])
            q3menus[key].triggered.connect(lambda checked, key=key : self.main_form.consavesv(q3menus[key].text()))
            x += 1

            # Vavle
        va = connect_menu.addMenu('Valve')
        vamenus = {}
        x = 0
        for element in vasv:
            key = 'V'+str(x)
            vamenus[key] = QAction(element[7], self)
            va.addAction(vamenus[key])
            vamenus[key].triggered.connect(lambda checked, key=key: self.main_form.consavesv(vamenus[key].text()))


        # Help Menu #
        help_bar = bar.addMenu('Help')
        source_menu = QAction('Get Source!', self)
        update_menu = QAction('Check for Updates', self)
        update_menu.setShortcut('Ctrl+U')
        about_menu = QAction('&About', self)

        help_bar.addAction(source_menu)
        help_bar.addAction(update_menu)
        help_bar.addAction(about_menu)

        # Menu Signals #
        exit_menu.triggered.connect(qApp.exit)
        update_menu.triggered.connect(self.main_form.chkupdate)
        about_menu.triggered.connect(self.main_form.showabout)
        source_menu.triggered.connect(self.main_form.get_src)

        self.setWindowTitle('RCon Plus')
        self.setWindowIcon(QIcon('RCon Plus125.png'))
        self.show()

app = QApplication(argv)
window = MainWin()
exit(app.exec_())

# RconPlus
Rcon Application, Written in Python3

Supports Quake 3 (Call of Duty series and ...) and Valve Rcon Protocols.

### How to Use:
1. Install Python3
2. Install required modules (`pip3 install --ModuleName--`)
- On Linux:
3. Open Terminal and enter `python3 RconPlus.py`
- On Windows:
3. Make sure u do have Python3 in your PATH, then open a CMD and enter `python RconPlus.py`

### Requirments:
- You need to install these modules => `webbrowser`, `python-valve`, `PyQt5`, `sip=4.19.4`

### To-Do:
- It does have a lot of bugs, I'll fix it soon...
- Work on Modularity
